import EmployeeList from './Components/FormList';
import EmployeeContextProvider from './Components/contexts/FromContext';

function App() {
  return (
    <div className="container-xl">
      <div className="table-responsive">
        <div className="table-wrapper">
          <EmployeeContextProvider>
            <EmployeeList />
          </EmployeeContextProvider>
        </div>
      </div>
    </div>

  );
}

export default App;