import { Form, Button } from "react-bootstrap"

import {EmployeeContext} from './contexts/FromContext';
import {useContext, useState} from 'react';

const EditForm = ({theEmployee}) =>{

    const id = theEmployee.id;
    const [salutation, setSalutation] = useState(theEmployee.salutation);
    const [firstname, setFirstName] = useState(theEmployee.firstname);
    const [lastname, setLastName] = useState(theEmployee.lastname);
    const [email, setEmail] = useState(theEmployee.email);
    const [date, setDate] = useState(theEmployee.date);
    const [address, setAddress] = useState(theEmployee.address);
    


    const {updateEmployee} = useContext(EmployeeContext);

    const updatedEmployee = {id, salutation, firstname, lastname, email, date, address}

    const handleSubmit = (e) => {
        e.preventDefault();
        updateEmployee(id, updatedEmployee)
    }

     return (

        <Form onSubmit={handleSubmit}>

            
<Form.Group> 
                    <label>Salutation:</label>
                <select  name="salutation" value={salutation}    onChange={(e)=> setSalutation(e.target.value)}>
                    <option  value="None" onChange={(e)=> setSalutation(e.target.value)}>None</option>
                    <option  value="text.." onChange={(e)=> setSalutation(e.target.value)}>1</option>
                    <option  value="eg1.."  onChange={(e)=> setSalutation(e.target.value)}>2..</option>
                    <option  value="eg2.." onChange={(e)=> setSalutation(e.target.value)}>3..</option>
                </select>
            </Form.Group>

            
            
            <Form.Group>
                <Form.Control
                    type="text"
                    placeholder=" First Name *"
                    name="firstname"
                    value={firstname}
                    onChange={(e)=> setFirstName(e.target.value)}
                    required
                />
            </Form.Group>
            <Form.Group>
                <Form.Control
                    type="text"
                    placeholder="Last Name *"
                    name="lastname"
                    value={lastname}
                    onChange={(e)=> setLastName(e.target.value)}
                    required
                />
            </Form.Group>
            <Form.Group>
                <Form.Control
                    type="email"
                    placeholder="Email *"
                    name="email"
                    value={email}
                    onChange={(e)=> setEmail(e.target.value)}
                    required
                />
            </Form.Group>
            <Form.Group>
                <Form.Control
                    type="date"
                    placeholder="Date of Birth"
                    name="date"
                    value={date}
                    onChange={(e)=> setDate(e.target.value)}
                />
            </Form.Group>
            <Form.Group>
                <Form.Control
                    as="textarea"
                    placeholder="Address"
                    rows={3}
                    name="address"
                    value={address}
                    onChange={(e)=> setAddress(e.target.value)}
                />
            </Form.Group>
            
            <Button variant="success" type="submit" block>
                Edit From
            </Button>

        </Form>

     )
}

export default EditForm;